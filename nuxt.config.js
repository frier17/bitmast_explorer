
module.exports = {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: 'Bitmast Explorer' || process.env.npm_package_name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' },
      {'http-equiv':"X-UA-Compatible", 'content':"IE=edge"},
      {name:"viewport", content:"width=device-width, initial-scale=1"},
      {name:"author", content:"Bitmast Digital Services"}
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: 'favicon.ico', sizes: "16x16"} ,
      { rel: 'stylesheet', type: 'image/x-icon', href: 'https://fonts.googleapis.com/css?family=Roboto|Baloo+Bhaina|Bungee+Outline|Comfortaa|Josefin+Sans|Source+Code+Pro'},
      {rel: 'stylesheet', type: 'text/css', href: 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'},
      {rel: 'stylesheet', type: 'text/css', href: 'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css'},
      {rel: 'stylesheet', type: 'text/css', href: 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css'}
    ],
    script: [
      {src:"https://code.jquery.com/jquery-3.4.1.min.js", integrity:"sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=", crossorigin:"anonymous"},
      {src:"https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js", crossorigin:"anonymous", type:"text/javascript"},
      {src:"https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js", type:"text/javascript"},
      {src:"https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js", type:"text/javascript"},
      {type:"text/javascript", src:"https://s3.tradingview.com/tv.js"} 
    ]
              
  },
  /*
  ** Customize the progress-bar color
  */
  loading: {     
failedColor: 'red',
height: '3px',
throttle: 50,
duration: 8000,
continuous: false,
    color: '#FFD700' 
  },
  /*
  ** Global CSS
  */
  css: [
    
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    
  ],
  devModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios'
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
