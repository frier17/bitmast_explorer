import axios from 'axios'


const fetchTradeData = async (sources, targets, relaxedValidation = false, extraParams = 'Bitmast Explorer', sign = false) => {
    const url = "https://min-api.cryptocompare.com/data/pricemulti";
    var output = {};
    // process top fiats
    
   let response = await axios.get(url, {
        params: {
          fsyms: sources.join(","),
          tsyms: targets.join(","),
          relaxedValidation,
          extraParams,
          sign
        }
      });
      
      if (response.status == 200 && response.hasOwnProperty("data")) {
        if (response.data.hasOwnProperty("Warning") || 
        response.data.hasOwnProperty("Response")) {
            output = {                
              isConverted: false,
              error: {
                networkError: false,
                applicationError: true,
                paramWithError: data.ParamWithError,
                warning: data.Warning,
                message: data.Message,
                payload: null
              }
            }
           throw Promise.reject(output);
          
        } else {
          output = {
              isConverted: true,
              tradePairs: response.data,
              error: null
          }
          return Promise.resolve(output);
        }
      } else if(response.status >= 400) {
        output = {
          isConverted: false,
          error: {networkError:  true, payload: error},
      }
      throw Promise.reject(output);
      } else {
        // application error occured
        output = {
          isConverted: false,
          error: {
                networkError: false,
                applicationError: true,                  
                payload: response.data
              }
        }
        throw Promise.reject(output);
      }      
      
};
export default fetchTradeData;